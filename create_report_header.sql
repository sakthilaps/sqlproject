
create table report_header(
	report_id int primary key,
	report_name varchar(50),
	status varchar(10) not null,
	user_id int,
	created_date timestamp not null,
	updated_date timestamp,
	created_by varchar(50) not null,
	updated_by varchar(50)
	
);
